# douane-daemon

This repository contains the daemon process, the brain, of [the Douane firewall](https://douaneapp.com), a Linux application firewall.

Find the compilation instructions in [the Compilation Wiki page](https://github.com/Douane/Douane/wiki/Compilation).

If you'd like to chat about Douane, join us on [![Gitter](https://badges.gitter.im/Douane/General.svg)](https://gitter.im/Douane/General?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)!